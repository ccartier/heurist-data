# Heurist-data

> This repository holds a number of python scripts that can retrieve and process Heurist records
> And transform the output as a CSV

## Data processing

1. Works that appear in the same manuscript
2. Manuscripts without a work
3. Editions without a work
4. Work in no manuscript
5. Works that overlap in manuscripts
