from .const import API, DB, LINK
from .basic_queries import get_url_response
from .utils import trim_html, write_csv

ps = {}

r = get_url_response(f"{API}?db={DB}&q=t%3A{LINK}&a=1&depth=all&linkmode=none&format=json&defs=0&extended=2")

for record in r['heurist']['records']:
    if record['rec_RecTypeID'] == f"{LINK}":
        metadata = record['details']

        id_ps = None
        work_title = None

        for meta in metadata:
            if meta['dty_ID'] == 936 or meta['dty_ID'] == 936:
                id_ps = meta['value']['id']
            elif meta['dty_ID'] == 935:
                work_title = trim_html(meta['value']['title'])

        if id_ps and work_title:
            if id_ps in ps.keys():
                ps[id_ps].append(work_title)
            else:
                ps[id_ps] = [work_title]

links = []
for ps_id in ps:
    for work in ps[ps_id]:
        links.append([ps_id, work])

write_csv(links)
