from setuptools import setup, find_packages

setup(
    name = "heurist",
    version = "1.0",
    packages = find_packages(),
)
